## About  

The Public Procurement Rulebook (PPRB) is the master data source for public procurement rules. This repository contains the public procurement rules dataset (rules registry), public procurement rules schema and documentation.   

The public procurement rules dataset has two main distributions. The master distribution comprises the collection of issues labeled with [rule label #TODO -- add the actual scoped label, once created]. Another distribution is in RDF format, conforming the PPRB schema [#TODO -- add link once comitted], developed and maintained in this repository. The RDF distribution will be published as part of the Public Procurement Dataspace RDF dataset, as a separate named graph. It will be read-only.   

PPRB registers technology-agnostic definitions of business rules in natural language. Creating a rule or providing feedback to a rule, does not require any specialised technical knowledge. Apart from rules definition, a rule entity in this registry should contain, when applicable, link to legal source of the rules and link to the entity representing the formal definition of the rule, done interoperable standard declarative language.  

This document is the specification of the PPRB. It describes the purpose, the use and the governance of PPRB.   

## Rules  

Rule are created by creating a new issue with template "rule" and filling in the title and rule definition. The automatically assigned rule label must not be changed. Other labels can be added as appropriate.  

### Mandatory fields  

The rule title should specify the rule in a clear and concise way. Rule title (issue title for issues with template "rule") is mandatory.  

The rule definition must be written in natural language. It should be clear, understandable and unambiguous. Although it is not a formally encoded rule description, it should be expressed in a way such that the executable rule logic can be derived from that. Rule definitions mandatory.  

### Optional fields  

Elaboration...  #TODO

## Issues  

Issue create with the "rule idea" or "rule" template are regular issues, questions, improvement proposals and so on related to rules or other artifacts of this repository.  

## PPRB in RDF  

#TODO